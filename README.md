# Welcome to my GitLab profile 👋

## ⚠️ Work in Progress ⚠️

Please note that this GitLab profile is a work in progress, and I am actively adding new content and improving existing sections. Feel free to explore my repositories, projects, and presentations, but keep in mind that some areas may be incomplete or under development.

Thank you for your understanding, and stay tuned for updates! 🚀

## About Me

I am a Mechanical Engineer with a Ph.D. in Computational Mechanics, passionate about solving complex problems through code. My GitLab profile is a collection of personal projects and code snippets that reflect my interests and expertise in various programming languages and computational tools.

## My Expertise

- **Programming Languages**: Python, Fortran, C, MATLAB

- **Scripting**: bash, awk

- **Data Science Libs**: Numpy, Pandas, SciPy

- **Visualization**: Matplotlib, Bokeh

## Get in Touch

- Connect with me on [LinkedIn](https://www.linkedin.com/in/nbiocca/)

<!---
- Email me at [youremail@example.com](mailto:youremail@example.com)
-->


